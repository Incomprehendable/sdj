![MAIN.png](https://bitbucket.org/repo/8MEjnR/images/934152231-MAIN.png)

Welcome to the official repository for SDJ, a Bukkit plugin by Incomprehendable. This plugin recreates double jumping into Minecraft. We're licensed under the GPL v3 license.