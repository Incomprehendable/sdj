package net.hailstormgaming.plugins.SDJ;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

@SuppressWarnings("unused")
public class SDJ extends JavaPlugin {
	private Plugin plg;
	
	
	public void onEnable(){
		plg = this;
		
	}
	public void onDisable(){
		plg = null;
	}
	
	private void $(String s){
		this.getServer().getLogger().info(s);
	}
	private void $(String s, boolean b){
		if(b){
			this.getServer().getLogger().severe(s);
		}else{
			this.getServer().getLogger().info(s);
		}
	}
	
	private void makeConfig(){
		
	}
	private void setListeners(){
		
	}
	private void registerEvents(Listener... li){
		for(Listener l : li){
			this.getServer().getPluginManager().registerEvents(l, this);
		}
	}
	
}
